import java.io.IOException;
import java.util.Random;

public class tstCaseRS {
    public static void main(String[] args) throws IOException,NullPointerException {
        try {
            final Random ran = new Random();
            Room room = new Room(1, 1, 1);
                for (int i = 0; i < 10; ++i) {
                    room.addFurniture(new Furniture(ran.nextInt(100), ran.nextInt(100), ran.nextInt(100)));
                }
            room.toJson("1.json");
            System.out.println(room.fromJson("1.json").toString());
            System.in.read();
        }
        catch(IOException e){System.out.println(e.getMessage().toString());}
        catch (NullPointerException e){System.out.println(e.getMessage().toString());}
    }
}

