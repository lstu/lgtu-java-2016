import javafx.scene.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.DrawMode;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;

public static class roomBuilder {
    public static Group stuff = new Group();
    public static PerspectiveCamera cam = new PerspectiveCamera();
    public static void addBox(int x, int y, int z, Color col, boolean fill)
    {
        Box b = new Box(75, 75, 75);
        b.setMaterial(new PhongMaterial(col));
        b.setDrawMode(fill? DrawMode.FILL:DrawMode.LINE);
        b.setTranslateX(x);
        b.setTranslateY(y);
        b.setTranslateZ(z);

        stuff.getChildren().add(b);
    }
    public static Parent buildRoom(){


        cam.getTransforms().addAll (
                new Rotate(-20, Rotate.Y_AXIS),
                new Rotate(-10, Rotate.X_AXIS),
                new Translate(+50, 0, -200));

        Text t = new Text(40, 80, "Test");
        t.setFont(new Font(40));
        t.setTranslateY(-50);
        t.setTranslateX(50);


        Group copyGroup = new Group();
        copyGroup.getChildren().add(stuff);
        copyGroup.getChildren().add(cam);
        copyGroup.getChildren().add(t);

        SubScene subScene = new SubScene(copyGroup, 300,300, true, SceneAntialiasing.BALANCED);
        subScene.setFill(Color.GHOSTWHITE);
        subScene.setCamera(cam);

        Group primary = new Group();

        primary.getChildren().add(subScene);
        return primary;
    }
}
