import java.io.*;
import javafx.scene.paint.Color;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Furniture extends Object implements IRoomSerialization {
    //координаты
    private double x, y, z;
    private Color color;
    private boolean fillStyle;
    private String label;

    public Furniture(){}
    public Furniture(double newX, double newY, double newZ) {
        this.x = newX;
        this.y = newY;
        this.z = newZ;
    }
    @Override
    public void toJson(String path) throws IOException {
        try (Writer writer = new OutputStreamWriter(new FileOutputStream(path), "UTF-8")) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            gson.toJson(this, writer);
        }
    }
    @Override
    public Furniture fromJson(String path) throws IOException {
        try(Reader reader = new InputStreamReader(new FileInputStream(path), "UTF-8"))
        {
            return new GsonBuilder().create().fromJson(reader, Furniture.class);
        }
    }
    @Override
    public String toString() {
        return new String("{typeof:"+ Furniture.class.toString()+" x: "+x+"px;\r\n y: "+ y +"px;\r\n z:" + z +"px;\r\n}");
    }


    //геттеры и сеттеры
    public double getX() {return this.x;}
    public double getY() {return this.y;}
    public double getZ() {return this.z;}
    public Color getColor() {return this.color;}
    public boolean getFillStyle(){return this.fillStyle;}
    public String getLabel() {return this.label;}

    public void setX(double newX){this.x = newX;}
    public void setY(double newY){this.y = newY;}
    public void setZ(double newZ){this.z = newZ;}
    public void setColor(Color newColor) {this.color = newColor;}
    public void setFillStyle(boolean newFillStyle){this.fillStyle = newFillStyle;}
    public void setLabel(String newLabel) {this.label = newLabel;}
}