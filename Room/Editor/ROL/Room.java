import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.Vector;
import java.io.*;

import java.io.IOException;

public class Room extends Object implements IRoomSerialization {

    private Integer height, width, depth;
    private Vector<Object>  list = new Vector<>();
    private Vector<String> textures = new Vector<>();
    public Room(Integer height, Integer width, Integer depth) {
        this.height = height;
        this.width = width;
        this.depth = depth;
    }
    public void setTexture(){}
    public void addTexture(String path) {
        try {
            textures.add(path);
        } catch(NullPointerException e){
            System.out.println(e.getMessage().toString());
        }
    }
    public void addFurniture(Furniture f) throws NullPointerException {
      try {
          list.add(f);
      } catch(NullPointerException e){
          System.out.println(e.getMessage().toString());
      }
    }
    public void remove(Integer index) throws ArrayIndexOutOfBoundsException {
        try {
            list.remove(index);
        }
        catch(Exception e){System.out.println(e.getMessage().toString());}
    }
    @Override
    public void toJson(String path) throws IOException{
        try (Writer writer = new OutputStreamWriter(new FileOutputStream(path), "UTF-8")) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            gson.toJson(this, writer);
        }
    }

    @Override
    public Room fromJson(String path) throws IOException{

        try(Reader reader = new InputStreamReader(new FileInputStream(path), "UTF-8"))
        {
            return new GsonBuilder().create().fromJson(reader, Room.class);
        }
    }
    @Override
    public String toString() {
        String output = new String("{typeof:"+ Room.class.toString()+" высота: "+height+"\n ширина: "+ width +"\n глубина/длина:" +depth+"\r\n}");
       for(Object obj:list) {
           output+="\r\n";
           output+=obj.toString();
       }
       return output;
    }
    public Integer getHeight() {return this.height;}
    public Integer getWidth() {return this.width;}
    public Integer getDepth() {return  this.depth;}
    public void setDepth(Integer depth){this.depth = depth;}
    public void setWidth(Integer width){this.width = width;}
    public void setHeight(Integer height){this.height = height;}
}
