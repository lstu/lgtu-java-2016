import java.io.IOException;

public interface IRoomSerialization {
void toJson(String Path) throws IOException;
Object fromJson(String path) throws IOException;
}
